import requests
from random import randint
from src.vote import Vote

url = "https://api.thecatapi.com/v1/"


def test_get_votes():
    #  Get votes
    resp = requests.get(f"{url}votes", headers={"x-api-key": "DEMO-API-KEY"})
    assert resp.status_code == 200
    assert len(resp.json()) > 0
    vote1 = Vote(resp.json()[0])
    assert vote1.id != 0


def test_get_vote_by_id():
    resp = requests.get(f"{url}votes", headers={"x-api-key": "DEMO-API-KEY"})
    assert resp.status_code == 200
    length = len(resp.json())
    assert length > 0

    vote1 = Vote(resp.json()[randint(0, length - 1)])
    assert vote1.id is not None

    # Get vote by id
    resp = requests.get(f"{url}votes/{vote1.id}", headers={"x-api-key": "DEMO-API-KEY"})
    assert resp.status_code == 200
    assert resp is not None

    assert vote1.id == resp.json()["id"]
    assert vote1.image_id == resp.json()["image_id"]
    assert vote1.sub_id == resp.json()["sub_id"]
    assert vote1.created_At == resp.json()["created_at"]
    assert vote1.value == resp.json()["value"]
    assert vote1.country_code == resp.json()["country_code"]


def test_create_delete_vote():
    payload = {"image_id": "asf2", "sub_id": "my-user-1234", "value": 1}

    resp = requests.post(f"{url}votes", headers={"x-api-key": "DEMO-API-KEY"}, json=payload)
    assert resp.status_code == 200
    assert resp.json()['message'] == "SUCCESS"
    vote_id = resp.json()['id']

    #  read
    resp = requests.get(f"{url}votes/{vote_id}", headers={"x-api-key": "DEMO-API-KEY"})
    assert resp.status_code == 200

    # delete
    resp = requests.delete(f"{url}votes/{vote_id}", headers={"x-api-key": "DEMO-API-KEY"})
    assert resp.status_code == 200
    assert resp.json()['message'] == "SUCCESS"

    #  read deleted
    resp = requests.get(f"{url}votes/{vote_id}", headers={"x-api-key": "DEMO-API-KEY"})
    assert resp.status_code == 404
    assert resp.json()['message'] == "NOT_FOUND"
