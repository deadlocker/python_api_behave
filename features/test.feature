Feature: showing off behave

  Scenario: Get votes
      When request votes
      Then response code is 200
      And votes number > 0
      When use vote with index 0
      Then vote id <> 0

  Scenario: Get vote by id
      When request votes
      And use random vote
      And request vote
      Then response code is 200
      And vote response match vote

  Scenario: Create delete vote
    When Post vote
    | image_id | sub_id       | value |
    | asf2     | my-user-1234 | 1     |
    Then response code is 200
    And response message is 'SUCCESS'
    And vote id saved
    When request vote
    Then response code is 200
    When delete vote
    Then response code is 200
    And response message is 'SUCCESS'
    When request vote
    Then response code is 404
    And response message is 'NOT_FOUND'

