from random import randint

import requests
from behave import *

from src.vote import Vote

url = "https://api.thecatapi.com/v1/"


@when('request votes')
def request_votes(context):
    context.resp = requests.get(f"{url}votes", headers={"x-api-key": "DEMO-API-KEY"})


@when('request vote')
def request_vote(context):
    context.resp = requests.get(f"{url}votes/{context.vote_id}", headers={"x-api-key": "DEMO-API-KEY"})


@then('votes number > 0')
def votes_not_empty(context):
    assert len(context.resp.json()) > 0


@then('vote id <> 0')
def votes_not_empty(context):
    assert context.vote.id != 0


@when('use vote with index {index}')
def set_vote_by_index(context, index):
    context.vote = Vote(context.resp.json()[int(index)])


@when('use random vote')
def set_random_vote(context):
    length = len(context.resp.json())
    context.vote = Vote(context.resp.json()[randint(0, length - 1)])
    context.vote_id = context.vote.id


@then('vote response match vote')
def vote_match_resp(context):
    vote_json = context.resp.json()
    assert context.vote.id == vote_json["id"]
    assert context.vote.image_id == vote_json["image_id"]
    assert context.vote.sub_id == vote_json["sub_id"]
    assert context.vote.created_At == vote_json["created_at"]
    assert context.vote.value == vote_json["value"]
    assert context.vote.country_code == vote_json["country_code"]


@when('Post vote')
def post_vote(context):
    row = context.table[0]
    payload = {"image_id": row['image_id'],
               "sub_id": row['sub_id'],
               "value": int(row['value'])}
    context.resp = requests.post(f"{url}votes", headers={"x-api-key": "DEMO-API-KEY"}, json=payload)


@then('response code is {code}')
def check_resp_code(context, code):
    assert context.resp.status_code == int(code)


@then("response message is '{msg}'")
def check_resp_message(context, msg):
    assert context.resp.json()['message'] == msg


@then("vote id saved")
def save_vote_id(context):
    context.vote_id = context.resp.json()['id']


@when("delete vote")
def delete_vote(context):
    context.resp = requests.delete(f"{url}votes/{context.vote_id}", headers={"x-api-key": "DEMO-API-KEY"})
