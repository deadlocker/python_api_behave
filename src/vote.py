class Vote:
    def __init__(self, json):
        self.id = json["id"]
        self.image_id = json["image_id"]
        self.sub_id = json["sub_id"]
        self.created_At = json["created_at"]
        self.value = json["value"]
        self.country_code = json["country_code"]
